# collect dirs to iterate
import csv
import glob
import itertools
import pickle as pk
import time
from collections import Counter

import copy
import numpy as np
import os
import scipy.stats as ss

from bench2cnf import simple_read_bench, tseytin_t
from util import cnf_to_matrix


def ent(data):
    """ input probabilities to get the entropy """
    entropy = ss.entropy(data)
    return entropy


if __name__ == '__main__':
    # parameter for reading data
    data_dir = 'data'
    benchmark = ['c432', 'c880']
    folder = ['rnd', 'skewl', 'skewh', 'cone', 'coneskewl', 'coneskewh', 'fanout', 'mainout']
    cnf_folder = list(itertools.product(benchmark, folder))

    start_time = time.time()

    # iterate benchmarks
    for c in benchmark:
        feat_list = []
        time_list = []
        for p in folder:
            print("Pre-processing {}/{} ...".format(c, p))

            # read runtime from report files
            time_dict = {}
            time_path = os.path.join(data_dir, 'reports', "{}_{}_LUT_.csv".format(c, p))
            if not os.path.isfile(time_path):
                continue
            for row in csv.DictReader(open(time_path)):
                time_dict[row['File name']] = row['Time taken']

            # read .bench file for generating CNF representation
            cnf_path = os.path.join(data_dir, c, p, 'replaced', '*')
            for f in glob.glob(cnf_path):
                print("File:{}".format(f), end='\r')
                file_name = f.split(os.sep)[-1]

                if file_name not in time_dict:
                    # print('==> Missing: {}'.format(file_name))
                    continue
                else:
                    time_list.append(float(time_dict[file_name]))

                feat = []

                # call function and extract CNF from .bench file
                wires = simple_read_bench(f)
                cnf_clause_count, cnf_content = tseytin_t(wires)
                # convert CNF to matrix form for follow up tasks
                incidence_mat = cnf_to_matrix(cnf_content)

                # check if there is zero rows
                zero_row_id = (incidence_mat == 0).all(1)
                incidence_mat = incidence_mat[~zero_row_id]  # remove all zero rows
                cnf_clause_count, cnf_variable_counts = incidence_mat.shape  # refresh var and clause num

                # derive literal degree and clause degree
                var_degree = np.sum(np.abs(incidence_mat), axis=0)
                clause_degree = np.sum(np.abs(incidence_mat), axis=1)

                # build literal graph and extract literal degree and features
                var_graph = np.zeros((cnf_variable_counts, cnf_variable_counts))
                for i in range(cnf_clause_count):
                    non_zero_list = np.where(incidence_mat[i] != 0)
                    pair_combo = [pair for pair in itertools.product(non_zero_list[0], repeat=2)]
                    for p in pair_combo:
                        var_graph[p] = 1
                var_graph_degree = np.sum(var_graph, axis=0)

                # extract positive and negative features from literal-clause graph
                positive_mat = copy.deepcopy(incidence_mat)
                positive_mat[positive_mat == -1] = 0

                negative_mat = copy.deepcopy(incidence_mat)
                negative_mat[negative_mat == 1] = 0
                negative_mat[negative_mat == -1] = 1

                positive_mat_0 = np.sum(positive_mat, axis=0)
                negative_mat_0 = np.sum(negative_mat, axis=0)
                positive_mat_1 = np.sum(positive_mat, axis=1)
                negative_mat_1 = np.sum(negative_mat, axis=1)
                p_ratio_0 = positive_mat_0 / (positive_mat_0 + negative_mat_0)
                n_ratio_0 = negative_mat_0 / (positive_mat_0 + negative_mat_0)
                p_ratio_1 = positive_mat_1 / (negative_mat_1 + positive_mat_1)
                n_ratio_1 = negative_mat_1 / (negative_mat_1 + positive_mat_1)

                # count ratio of binary and ternary clause
                bin_tern_cnt = Counter(np.sum(np.abs(incidence_mat), axis=1))
                bin_ratio = bin_tern_cnt[2] / sum(bin_tern_cnt.values())
                tern_ratio = bin_tern_cnt[3] / sum(bin_tern_cnt.values())

                feat.append(cnf_variable_counts)  # 0
                feat.append(cnf_clause_count)  # 1
                feat.append(float(cnf_clause_count / cnf_variable_counts))  # 2

                # extract entropy for each feature
                feat.append(ent(var_degree))
                feat.append(ent(clause_degree))
                feat.append(ent(var_graph_degree))
                feat.append(ent(p_ratio_0))
                feat.append(ent(p_ratio_1))
                feat.append(ent(n_ratio_0))
                feat.append(ent(n_ratio_1))

                # prepare for energy calculation
                feat.append([var_degree])
                feat.append([clause_degree])
                feat.append([var_graph_degree])
                feat.append([p_ratio_0])
                feat.append([p_ratio_1])
                feat.append([n_ratio_0])
                feat.append([n_ratio_1])

                feat_list.append(feat)

        # save data
        pk.dump(feat_list, open('./preprocess_data/{}_X.pk'.format(c), 'wb'))
        pk.dump(time_list, open('./preprocess_data/{}_Y.pk'.format(c), 'wb'))

    print("Total time elapsed: {:.4f}s".format(time.time() - start_time))
