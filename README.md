# Intro
This is a PyTorch implementation of CNFNet for the task of estimating deobfuscation time of given obfuscated circuit.

# Quick start
Our codes are evaluated under python 3.6

## prepare environment
install [virtualenv](https://virtualenv.pypa.io/en/latest/installation/) and create a virtual environment called cnfnet
```
virtualenv --python=python3.6 cnfnet
```
install required python packages
```
pip install -r requirements.txt  
```

## pre-process data
two datasets are provided in `data` folder
create a folder for storing preprocess data
```
mkdir preprocess_data 
```

run preprocess program
```
python cnf_preprocess.py 
```

## run the main program:
```
python run.py  
```

# Data
For the experimental setup, we have used the [ISCAS-89 benchmarks](http://www.pld.ttu.ee/~maksim/benchmarks/iscas85/verilog/) , and obfuscation instances on 6 different circuits are selected as 6 benchmark datasets (c432, c499, c880, c1355, c1908 and c2670). 
These benchmarks are synthesized using the Synopsys DC Compiler with the help of 32/28nm [Generic library](https://www.synopsys.com). The synthesized netlist is further converted to the bench format as per the [SAT-solver](http://www.eecs.umich.edu/courses/eecs578/eecs578.f15/papers/sub15.pdf) requirements.
Two benchmarks are provided in this repo

# Model
The steps of CNFNet:
1. CNF to Bipartite graph

2. graph to multi-order

3. multi-order to distribution

4. distribution to energy


# Files usage
## main program
- `run.py`: training CNFNet and test on random instances.
- `model.py` and `layer.py`: model's operation

## preprocessing program
- `bench2cnf.py`: generate Circuit-CNF representation given a circuit
- `cnf_preprocess.py`: extract initial features for training
- `util.py`: set of auxiliary functions
- `data`: demo data which include two datasets




