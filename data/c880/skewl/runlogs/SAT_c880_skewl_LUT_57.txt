inputs=60 keys=196 outputs=26 gates=923
iteration: 1; vars: 4045; clauses: 1941; decisions: 1720
iteration: 2; vars: 4923; clauses: 2946; decisions: 2693
iteration: 3; vars: 5801; clauses: 4092; decisions: 4197
iteration: 4; vars: 6679; clauses: 5036; decisions: 5720
iteration: 5; vars: 7557; clauses: 2225; decisions: 6515
iteration: 6; vars: 8435; clauses: 3363; decisions: 7629
iteration: 7; vars: 9313; clauses: 4535; decisions: 8263
iteration: 8; vars: 10191; clauses: 2584; decisions: 9465
iteration: 9; vars: 11069; clauses: 3551; decisions: 10104
iteration: 10; vars: 11947; clauses: 4593; decisions: 11046
iteration: 11; vars: 12825; clauses: 2933; decisions: 13120
iteration: 12; vars: 13703; clauses: 4101; decisions: 14419
iteration: 13; vars: 14581; clauses: 5316; decisions: 16669
iteration: 14; vars: 15459; clauses: 6313; decisions: 17083
iteration: 15; vars: 16337; clauses: 3122; decisions: 19079
iteration: 16; vars: 17215; clauses: 3968; decisions: 20043
iteration: 17; vars: 18093; clauses: 4921; decisions: 21010
iteration: 18; vars: 18971; clauses: 5962; decisions: 21877
iteration: 19; vars: 19849; clauses: 3550; decisions: 22359
iteration: 20; vars: 20727; clauses: 4478; decisions: 23710
iteration: 21; vars: 21605; clauses: 5336; decisions: 24488
iteration: 22; vars: 22483; clauses: 6436; decisions: 24841
iteration: 23; vars: 23361; clauses: 3974; decisions: 25221
iteration: 24; vars: 24239; clauses: 5058; decisions: 26050
iteration: 25; vars: 25117; clauses: 6131; decisions: 26880
iteration: 26; vars: 25995; clauses: 7130; decisions: 27276
iteration: 27; vars: 26873; clauses: 8246; decisions: 27899
iteration: 28; vars: 27751; clauses: 4342; decisions: 30221
iteration: 29; vars: 28629; clauses: 5270; decisions: 30685
iteration: 30; vars: 29507; clauses: 6221; decisions: 31692
iteration: 31; vars: 30385; clauses: 7154; decisions: 33410
iteration: 32; vars: 31263; clauses: 8131; decisions: 34694
iteration: 33; vars: 32141; clauses: 9266; decisions: 35111
iteration: 34; vars: 33019; clauses: 4993; decisions: 38207
iteration: 35; vars: 33897; clauses: 6082; decisions: 39163
iteration: 36; vars: 34775; clauses: 7068; decisions: 40515
iteration: 37; vars: 35653; clauses: 8050; decisions: 40871
iteration: 38; vars: 36531; clauses: 9038; decisions: 42289
iteration: 39; vars: 37409; clauses: 10005; decisions: 42898
iteration: 40; vars: 38287; clauses: 5595; decisions: 43398
iteration: 41; vars: 39165; clauses: 6589; decisions: 44599
iteration: 42; vars: 40043; clauses: 7604; decisions: 47901
iteration: 43; vars: 40921; clauses: 8549; decisions: 54521
finished solver loop. fail_count = 0
key=1001100001011000011010000001011000010001101010101010101001101001011010000110000100010110000110101010011001110001000100010001000100010001000101011000100010001000100010001000100010001000100010001000
iteration=44; backbones_count=0; cube_count=213602; cpu_time=1.58766; maxrss=11.7773
