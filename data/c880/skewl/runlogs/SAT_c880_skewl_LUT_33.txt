inputs=60 keys=120 outputs=26 gates=725
iteration: 1; vars: 3101; clauses: 1483; decisions: 3347
iteration: 2; vars: 3781; clauses: 2130; decisions: 3812
iteration: 3; vars: 4461; clauses: 2740; decisions: 4258
iteration: 4; vars: 5141; clauses: 3491; decisions: 4751
iteration: 5; vars: 5821; clauses: 1590; decisions: 5177
iteration: 6; vars: 6501; clauses: 2286; decisions: 5615
iteration: 7; vars: 7181; clauses: 2990; decisions: 6045
iteration: 8; vars: 7861; clauses: 3692; decisions: 6543
iteration: 9; vars: 8541; clauses: 4417; decisions: 6967
iteration: 10; vars: 9221; clauses: 5165; decisions: 7245
iteration: 11; vars: 9901; clauses: 1758; decisions: 8003
iteration: 12; vars: 10581; clauses: 2222; decisions: 8256
iteration: 13; vars: 11261; clauses: 2689; decisions: 8517
iteration: 14; vars: 11941; clauses: 3244; decisions: 8879
iteration: 15; vars: 12621; clauses: 3984; decisions: 9294
iteration: 16; vars: 13301; clauses: 4702; decisions: 9650
iteration: 17; vars: 13981; clauses: 5293; decisions: 10261
iteration: 18; vars: 14661; clauses: 1917; decisions: 10842
iteration: 19; vars: 15341; clauses: 2496; decisions: 11421
iteration: 20; vars: 16021; clauses: 3165; decisions: 11690
iteration: 21; vars: 16701; clauses: 3717; decisions: 11922
iteration: 22; vars: 17381; clauses: 4287; decisions: 12159
iteration: 23; vars: 18061; clauses: 4804; decisions: 12468
iteration: 24; vars: 18741; clauses: 5289; decisions: 13691
iteration: 25; vars: 19421; clauses: 2028; decisions: 14610
iteration: 26; vars: 20101; clauses: 2599; decisions: 15681
iteration: 27; vars: 20781; clauses: 3290; decisions: 16676
finished solver loop. fail_count = 0
key=101001101000011010000110011000010001011010100110011000010001000100010001000100010001101010001000100010001000100010001000
iteration=28; backbones_count=0; cube_count=104800; cpu_time=0.394766; maxrss=7.63281
