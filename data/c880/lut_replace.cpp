#include <stdint.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <thread>
#include <termios.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string.h>
#include <math.h>
#include <time.h>

using namespace std;

#define LOG_LEVEL_INFO		1
#define LOG_LEVEL_DEBUG		2

#define LOG_LEVEL	LOG_LEVEL_INFO
//#define LOG_LEVEL	LOG_LEVEL_DEBUG

#if LOG_LEVEL >= LOG_LEVEL_DEBUG
	#define LOG_DEBUG(arg)	cout << arg
#else
	#define LOG_DEBUG(arg)
#endif

#if LOG_LEVEL >= LOG_LEVEL_INFO
	#define LOG_INFO(arg)	cout << arg
#else
	#define LOG_INFO(arg)
#endif

#define NO_ERR				0
#define BAD_INPUT_FILE 		-1
#define WRONG_BENCHMARK		-2
#define FALSE_REPLACEMENT	-3

#define MAXLINE 40

int LUT_replacement(string benchmark_address, string gate_list[], int size_gate_list);
string out_gate_change(string main_line, int operand_cnt);

string add_key_inputs(string main_gate_line, int operand_cnt, int key_offset);
string add_key_logic(string main_gate_line, int operand_cnt, int key_offset);

void array_strcpy(string dst_array[], string src_array[], int size);
void array_str_print(string array[], int size);
int gate_opr_counter(string line);

void usage(void);
int calc_gate_cnt(string benchmark_address, float percent);
int replace_marked_gates(string benchmark_address);

int main(int argc, char **argv)
{

	int err_no;	

	string benchmark_address;
	float percent;

	int option;

	while ((option = getopt (argc, argv, "b:p:h")) != -1)
	{
		switch (option)
		{
			case 'b':
				benchmark_address = optarg;
				break;
			case 'p':
				percent = atof(optarg);
				break;
			case 'h':
				usage();
				return NO_ERR;
				break;	
			default:
				usage();
				return NO_ERR;	
		}

	}
	
	LOG_DEBUG("The address of benchmark is: " << benchmark_address << endl);
	LOG_DEBUG("The percentage of obfuscating in this benchmark is: " << percent << endl);

	err_no = replace_marked_gates(benchmark_address);
	if(err_no != NO_ERR)
	{
		LOG_INFO("LUT replacement is failed. err_no: " << err_no << endl);
		return NO_ERR;
	}

	LOG_INFO("LUT replacement is done." << endl);
	return NO_ERR;
}

int LUT_replacement(string benchmark_address, string gate_list[], int size_gate_list)
{
	LOG_DEBUG("Starting LUT_replacement()" << endl);
	
	int size_to_be_obfuscated = size_gate_list;
	string to_be_obfuscated[size_to_be_obfuscated];
	array_strcpy(to_be_obfuscated, gate_list, size_to_be_obfuscated);
	LOG_DEBUG("The gates which should be obfuscated are:" << endl);
	array_str_print(to_be_obfuscated, size_to_be_obfuscated);

	int iter = 0;

	int key_offset = 0;

	string main_line;

	int operand_cnt;
	string main_gate_line;

	string comments = "";

	string main_inputs = "";
	string key_inputs = "";

	string main_outputs = "";

	string main_logic = "";
	string key_logic = "";

	string new_netlist = "";


	string main_gate;

	size_t found_main_input;
	size_t found_main_output;
	size_t found_main_gate;

	fstream bench_file(benchmark_address);

	LOG_DEBUG("The benchmark address for obfuscating is:" << endl << benchmark_address << endl);
	LOG_DEBUG(size_to_be_obfuscated << " gates should be obfuscated." << endl);
	while(size_to_be_obfuscated != 0)
	{
		comments = "";
		main_inputs = "";
		key_inputs = "";
		main_outputs = "";
		main_logic = "";
		key_logic = "";

		main_gate = to_be_obfuscated[iter];
		LOG_DEBUG("The gate which should be obfuscated in this iteration is: " << to_be_obfuscated[0] << endl);
		bench_file.open(benchmark_address);
        bench_file.close();
        bench_file.open(benchmark_address);	

		if(bench_file.is_open())
		{
			LOG_DEBUG("reading line from the main benchmark file" << endl);
			while(getline(bench_file,main_line))
			{
				LOG_DEBUG(main_line << endl);
				found_main_input = main_line.find("INPUT");
				found_main_output = main_line.find("OUTPUT");
				found_main_gate = main_line.find(main_gate+" =");
				if (main_line[0]=='#') 
				{
					LOG_DEBUG("add this line to the comment lines:   " << main_line << endl);
					comments += main_line+'\n';
				}
				else if(found_main_input != std::string::npos)
				{
					LOG_DEBUG("add this line to main_inputs:   " << main_line << endl);
					main_inputs += main_line+'\n'; 
				}				
				else if(found_main_output != std::string::npos)
				{
					LOG_DEBUG("add this line to main_outputs:   " << main_line << endl);
					main_outputs += main_line+'\n'; 
				}				
				else if(found_main_gate != std::string::npos)
				{
					LOG_DEBUG("change the gate (should be obfuscated) with last gate in LUT:   " << main_line << endl);
					operand_cnt = gate_opr_counter(main_line);
					main_gate_line = main_line;
					main_line = out_gate_change(main_line, operand_cnt);
					LOG_DEBUG("the new line is:   " << main_line << endl);
					main_logic += main_line+'\n';
				}
				else
				{
					LOG_DEBUG("add this line without change to final version:   " << main_line << endl);
					main_logic += main_line+'\n';
				}
			}
		bench_file.close();	
		}
		else
		{
			LOG_INFO("BAD INPUT FILE. " << endl);
			return BAD_INPUT_FILE;
		}

		key_inputs = add_key_inputs(main_gate_line, operand_cnt, key_offset);
		key_logic =  add_key_logic(main_gate_line, operand_cnt, key_offset);

		LOG_DEBUG("=========== comments ===========" << endl << comments << endl);
		LOG_DEBUG("=========== main inputs ===========" << endl << main_inputs << endl);
		LOG_DEBUG("=========== key inputs ===========" << endl << key_inputs << endl);
		LOG_DEBUG("=========== main outputs ===========" << endl << main_outputs << endl);
		LOG_DEBUG("=========== main logic ===========" << endl << main_logic << endl);
		LOG_DEBUG("=========== key logic ===========" << endl << key_logic << endl);
		
		new_netlist = comments + "\n" + main_inputs + key_inputs + "\n" + main_outputs + main_logic + key_logic;

		LOG_DEBUG("=========== new netlist ===========" << endl << new_netlist << endl);

		bench_file.open(benchmark_address);
		bench_file << new_netlist;
		bench_file.close();

		iter++;
		size_to_be_obfuscated--;
		key_offset += pow(2, operand_cnt);
	}



	LOG_DEBUG("Finishing LUT_replacement()" << endl);
	return NO_ERR;
}

string out_gate_change(string main_line, int operand_cnt)
{
	LOG_DEBUG("starting out_gate_change() for " << main_line << endl);
	string current_line = "";

	string delimiter1 = " =";
	size_t pos_var; 
	string output_var;

	

	pos_var = main_line.find(delimiter1);
	output_var = main_line.substr(0, pos_var);

	LOG_DEBUG("the output of this is: " << output_var << endl);
	LOG_DEBUG("the nummber of operands in this gate is: " << operand_cnt << endl);

	switch(operand_cnt) 
	  	{
		    case 1:
		    	current_line = output_var+" = or("+output_var+"$enc1, "+output_var+"$enc2)";
		    	LOG_DEBUG("the new generated line is: " << current_line << endl);
		    	break;
		    case 2:
		    	current_line = output_var+" = or("+output_var+"$enc10, "+output_var+"$enc11)";
		    	LOG_DEBUG("the new generated line is: " << current_line << endl);
		    	break;
		    case 3:
		    	current_line = output_var+" = or("+output_var+"$enc21, "+output_var+"$enc22)";
		    	LOG_DEBUG("the new generated line is: " << current_line << endl);
		    	break;
		    case 4:
		    	current_line = output_var+" = or("+output_var+"$enc44, "+output_var+"$enc45)";
		    	LOG_DEBUG("the new generated line is: " << current_line << endl);
		    	break;
		    case 5:
		    	current_line = output_var+" = or("+output_var+"$enc85, "+output_var+"$enc86)";
		    	LOG_DEBUG("the new generated line is: " << current_line << endl);
		    	break;
	    	default:
	    	;
		    	
		}


	return current_line;
}

string add_key_inputs(string main_gate_line, int operand_cnt, int key_offset)
{
	string key_inputs = "";
	LOG_DEBUG("add new key inputs based on this gate : " << main_gate_line << endl);
	for(int i=0 ; i<pow(2,operand_cnt) ; i++)
	{
		LOG_DEBUG("add new key input : " << "INPUT(keyinput"+to_string(i+key_offset)+")" << endl);
		key_inputs += "INPUT(keyinput"+to_string(i+key_offset)+")\n";
	}
	return key_inputs;
}

string add_key_logic(string main_gate_line, int operand_cnt, int key_offset)
{
	LOG_DEBUG("add new key logic based on this gate : " << main_gate_line << endl);
	string key_logic = "";

	string gate_inputs = "";

	string gate_input_list[operand_cnt];
	size_t pos_st_par;
	size_t pos_end_par;
	size_t pos_divide;
	size_t pos_var; 
	string output_var;

	int index_enc = 0;

	string delimiter1 = "(";
	string delimiter2 = ")";
	string delimiter3 = ", ";
	string delimiter4 = " =";

	pos_st_par = main_gate_line.find(delimiter1) + 1;
	pos_end_par = main_gate_line.find(delimiter2) - 1;

	pos_var = main_gate_line.find(delimiter4);
	output_var = main_gate_line.substr(0, pos_var);

	LOG_DEBUG("the output of this is: " << output_var << endl);
	LOG_DEBUG("the nummber of operands in this gate is: " << operand_cnt << endl);

	LOG_DEBUG("the positions of the par_st and par_end are: " << pos_st_par << " & " << pos_end_par << endl);

	if((pos_st_par!=std::string::npos) && (pos_end_par!=std::string::npos))
	{
		gate_inputs = main_gate_line.substr(pos_st_par,pos_end_par);
		gate_inputs = gate_inputs.substr(0,gate_inputs.length()-1);
		LOG_DEBUG("the inputs operand on this gate is: " << gate_inputs << endl);
	}

	for(int i=0 ; i<operand_cnt ; i++)
	{
		if(operand_cnt == 1)
		{
			gate_input_list[i] = gate_inputs;
			LOG_DEBUG("the only operand in this gate is: " << gate_input_list[i] << endl);
		}
		else if(i != operand_cnt-1)
		{
			pos_divide = gate_inputs.find(delimiter3);
			if(pos_divide!=std::string::npos)
			{
				gate_input_list[i] = gate_inputs.substr((i!=0)*1, pos_divide-1*(i!=0));
				LOG_DEBUG("one of operand in this gate is: " << gate_input_list[i] << endl);
				gate_inputs.erase(0, pos_divide + delimiter1.length());
				LOG_DEBUG("the remaining operands in this gate is: " << gate_inputs << endl);
			}
		}
		else
		{
				gate_input_list[i] = gate_inputs.substr(1, gate_inputs.length()-1);
				LOG_DEBUG("the last operand in this gate is: " << gate_input_list[i] << endl);
		}
	}

	for(int i=0 ; i<operand_cnt ; i++)
	{
		key_logic += output_var+"$enc"+to_string(i)+" = not("+gate_input_list[i]+")\n";
	}
	index_enc = operand_cnt;
	
	if(operand_cnt == 1)
	{
		key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(index_enc-1)+", keyinput"+to_string(key_offset)+")\n";
    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[0]+", keyinput"+to_string(key_offset+1)+")\n";
	}
	else
	{
		key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(1)+", "+output_var+"$enc"+to_string(0)+")\n";
    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(1)+", "+gate_input_list[0]+")\n";
    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[1]+", "+output_var+"$enc"+to_string(0)+")\n";
    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[1]+", "+gate_input_list[0]+")\n";
	}

	if(operand_cnt > 1)
	{
		for(int i=0 ; i<pow(2,operand_cnt) ; i++)
		{
			key_logic += output_var+"$enc"+to_string(i+index_enc)+" = and("+output_var+"$enc"+to_string(index_enc+i-4-(int)floor(i/4)*4)+", keyinput"+to_string(key_offset+i)+")\n";
		}
		index_enc = index_enc + pow(2,operand_cnt);
	}
	
	if(operand_cnt > 1)
	{
		for(int i=0 ; i<pow(2,operand_cnt-1) ; i++)
    	{
    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = or("+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt))+", "+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt)+1)+")\n";	
    	}
    	index_enc = index_enc + pow(2,operand_cnt-1);
	}

	if(operand_cnt > 2)
	{
		for(int i=0 ; i<pow(2,operand_cnt-2) ; i++)
    	{
    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = or("+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-1))+", "+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-1)+1)+")\n";	
    	}
    	index_enc = index_enc + pow(2,operand_cnt-2);
	}

	switch(operand_cnt) 
  	{
	    case 3:
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(index_enc-2)+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(index_enc-2)+", "+gate_input_list[2]			+")\n";
	    	break;
	    case 4:
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(3)	+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[3]				+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(3)	+", "+gate_input_list[2]			+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[3]				+", "+gate_input_list[2]			+")\n";
	    	for(int i=0 ; i<pow(2,operand_cnt-2) ; i++)
	    	{
	    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = and("+output_var+"$enc"+to_string(index_enc+i-(int)pow(2,operand_cnt-2))+", "+output_var+"$enc"+to_string(index_enc+i-(int)pow(2,operand_cnt-1))+")\n";	
	    	}
	    	index_enc = index_enc + pow(2,operand_cnt-2);

	    	for(int i=0 ; i<pow(2,operand_cnt-3) ; i++)
	    	{
	    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = or("+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-2))+", "+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-2)+1)+")\n";	
	    	}
	    	index_enc = index_enc + pow(2,operand_cnt-3);

	    	break;
	    case 5:
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(4)	+", "+output_var+"$enc"+to_string(3)+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[4]				+", "+output_var+"$enc"+to_string(3)+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(4)	+", "+gate_input_list[3]			+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[4]				+", "+gate_input_list[3]			+", "+output_var+"$enc"+to_string(2)+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(4)	+", "+output_var+"$enc"+to_string(3)+", "+gate_input_list[2]			+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[4]				+", "+output_var+"$enc"+to_string(3)+", "+gate_input_list[2]			+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+output_var+"$enc"+to_string(4)	+", "+gate_input_list[3]			+", "+gate_input_list[2]			+")\n";
	    	key_logic += output_var+"$enc"+to_string(index_enc++)+" = and("+gate_input_list[4]				+", "+gate_input_list[3]			+", "+gate_input_list[2]			+")\n";


	    	for(int i=0 ; i<pow(2,operand_cnt-2) ; i++)
	    	{
	    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = and("+output_var+"$enc"+to_string(index_enc+i-(int)pow(2,operand_cnt-2))+", "+output_var+"$enc"+to_string(index_enc+i-(int)pow(2,operand_cnt-1))+")\n";	
	    	}
	    	index_enc = index_enc + pow(2,operand_cnt-2);

	    	for(int i=0 ; i<pow(2,operand_cnt-3) ; i++)
	    	{
	    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = or("+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-2))+", "+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-2)+1)+")\n";	
	    	}
	    	index_enc = index_enc + pow(2,operand_cnt-3);

	    	for(int i=0 ; i<pow(2,operand_cnt-4) ; i++)
	    	{
	    		key_logic += output_var+"$enc"+to_string(index_enc+i)+" = or("+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-3))+", "+output_var+"$enc"+to_string(index_enc+2*i-(int)pow(2,operand_cnt-3)+1)+")\n";	
	    	}
	    	index_enc = index_enc + pow(2,operand_cnt-4);

	    	break;
    	default:
    	; 	
	}

	return key_logic;
}

void array_strcpy(string dst_array[], string src_array[], int size)
{
	for(int i=0 ; i<size ; i++)
	{
		dst_array[i] = src_array[i];
	}
}

void array_str_print(string array[], int size)
{
	for(int i=0 ; i<size ; i++)
	{
		LOG_DEBUG(array[i] << endl);
	}
}

int gate_opr_counter(string line)
{
	int cnt_gate = 1;
	int i = 0; 
		while(i < line.length())
		{
			if(line[i] == ',')
			{
				cnt_gate++;
			}
			i++;
		}
	return cnt_gate;
}

int calc_gate_cnt(string benchmark_address, float percent)
{
	size_t found_bench_name;
	found_bench_name = benchmark_address.find("c17.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C17" << endl);
		LOG_DEBUG("The number of gates in C17 is: 6" << endl);
		return 6;
	}

	found_bench_name = benchmark_address.find("c432.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C432" << endl);
		LOG_DEBUG("The number of gates in C432 is: 120" << endl);
		return 120;
	}

	found_bench_name = benchmark_address.find("c499.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C499" << endl);
		LOG_DEBUG("The number of gates in C499 is: 162" << endl);
		return 162;
	}

	found_bench_name = benchmark_address.find("c880.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C880" << endl);
		LOG_DEBUG("The number of gates in C880 is: 320" << endl);
		return 320;
	}

	found_bench_name = benchmark_address.find("c1355.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C1355" << endl);
		LOG_DEBUG("The number of gates in C1355 is: 506" << endl);
		return 506;
	}

	found_bench_name = benchmark_address.find("c1908.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C1908" << endl);
		LOG_DEBUG("The number of gates in C1908 is: 603" << endl);
		return 603;
	}

	found_bench_name = benchmark_address.find("c2670.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C2670" << endl);
		LOG_DEBUG("The number of gates in C2670 is: 872" << endl);
		return 872;
	}

	found_bench_name = benchmark_address.find("c3540.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C3540" << endl);
		LOG_DEBUG("The number of gates in C3540 is: 1179" << endl);
		return 1179;
	}

	found_bench_name = benchmark_address.find("c5315.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C5315" << endl);
		LOG_DEBUG("The number of gates in C5315 is: 1726" << endl);
		return 1726;
	}

	found_bench_name = benchmark_address.find("c6288.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C6288" << endl);
		LOG_DEBUG("The number of gates in C6288 is: 2384" << endl);
		return 2384;
	}

	found_bench_name = benchmark_address.find("c7552.bench");
	if(found_bench_name != std::string::npos)
	{
		LOG_DEBUG("The benchmark is: C7552" << endl);
		LOG_DEBUG("The number of gates in C7552 is: 2636" << endl);
		return 2636;
	}
	else
	{
		LOG_DEBUG("The circuit " << benchmark_address << " is not defined in the DB." << endl);
		return WRONG_BENCHMARK;
	}
}


int replace_marked_gates(string benchmark_address)
{
	string gate_list[1000] = "";
	int index_gate = 0;

	string main_line;
	size_t found_gate;

	size_t pos_ind_par;
	size_t pos_ind_eq;

	string delimiter_marked = "marked";
	size_t found_marked;

	string gate_type;


	fstream bench_file(benchmark_address);

	bench_file.open(benchmark_address);
    bench_file.close();
    bench_file.open(benchmark_address);
    if(bench_file.is_open())
	{
		while(getline(bench_file,main_line))
		{
			found_gate = main_line.find(" =");
			if(found_gate != std::string::npos)
			{
				LOG_DEBUG("Current Gate: " << main_line << endl);

				found_marked = main_line.find(delimiter_marked);
				if(found_marked != std::string::npos)
				{
					gate_list[index_gate] = main_line.substr(0, main_line.find(" ="));
					index_gate++;
					LOG_DEBUG("The number of gates in the list is: " << index_gate << endl);
				}
			}
		}
		bench_file.close();	
	}

	string final_gate_list[index_gate];
	LOG_DEBUG("The list of gates should be replaced by LUTs is: " << endl);
	array_strcpy(final_gate_list, gate_list, index_gate);
	array_str_print(final_gate_list, index_gate);

	LOG_DEBUG("Calling LUT_replacement()" << endl);
	int err_no = LUT_replacement(benchmark_address, final_gate_list, index_gate);
	LOG_DEBUG("Returning LUT_replacement()" << endl);
	if(err_no != NO_ERR)
	{
		LOG_INFO("LUT_replacement is failed. err_no is " << NO_ERR);
		return FALSE_REPLACEMENT;
	}
	else
	{
		LOG_INFO("LUT_replacement is passed. " << endl);
		return NO_ERR;
	}

}

void usage(void)
{
    printf("Usage:\n");
    printf(" -b\t\t benchmark address\n");
    printf(" -p\t\t percentage of obfuscation (range: 0-1)\n");
    printf(" -h\t\t Help\n\n");
}
