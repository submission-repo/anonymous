import argparse
import copy
import operator
from random import randint

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Result Extractor (c) Hadi Mardani Kamali.')
    parser.add_argument("-b", action="store", required=True, type=str, help="benchmark circuit")
    parser.add_argument("-m", action="store", required=True, help="marking method")
    parser.add_argument("-n", action="store", required=True, type=int, help="number of markings")

    args = parser.parse_args()

    bench_circuit = args.b
    marking_policy = args.m

    marked_bench_address = ""
    sat_exe_time = ""

    found = 0
    obfuscated_gates = ""

    report_content = "File name,Time taken,obfuscated Gates\n"

    for i in range(1, args.n):
        marked_bench_address = "marked/" + bench_circuit + "_" + marking_policy + "_LUT_" + str(i) + ".bench"
        marked_bench_file = open(marked_bench_address)

        for line in marked_bench_file:
            if " = " in line:
                if "marked" in line:
                    obfuscated_gates += line[0: line.find(" =")] + ","

        obfuscated_gates = obfuscated_gates[0:len(obfuscated_gates)-1]

        sat_exe_time = "runlogs/SAT_" + bench_circuit + "_" + marking_policy + "_LUT_" + str(i) + ".txt"

        sat_exe_file = open(sat_exe_time)

        for line in sat_exe_file:
            if "cpu_time=" in line:
                found = 1
                current_exe_time = line[line.find("time=") + 5: line.find("; max")]

        if found:
            report_content += bench_circuit + "_" + marking_policy + "_LUT_" + str(i) + ".bench," + current_exe_time + ", " + obfuscated_gates + "\n"
            found = 0
            obfuscated_gates = ""

        report_bench_address = bench_circuit + "_" + marking_policy + "_LUT_" + ".csv"
        new_bench_file = open(report_bench_address, 'w')
        new_bench_file.write(report_content)
        new_bench_file.close()

    print("the report has been sucessfully written into ", bench_circuit + "_" + marking_policy + "_LUT_" + ".csv")
