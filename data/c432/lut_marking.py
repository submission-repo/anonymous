import argparse
import copy
import operator
from random import randint

class wire:
    def __init__(self, name, type, operands, logic_value, logic_level, prob0, prob1, absprob, fanout, mainout):
        self.name = name
        self.type = type
        self.operands = operands
        self.logic_value = logic_value
        self.logic_level = logic_level
        self.prob0 = prob0
        self.prob1 = prob1
        self.absprob = absprob
        self.fanout = fanout
        self.mainout = mainout


def unique(a):
    """ return the list with duplicate elements removed """
    return list(set(a))


def wire_print(wire_in):
    print("w_name: ", wire_in.name)
    print("w_type: ", wire_in.type)
    for i in range(0, len(wire_in.operands)):
        print("w_opr", i, ": ", wire_in.operands[i].name)
    print("w_valu: ", wire_in.logic_value)
    print("w_glev: ", wire_in.logic_level)
    print("w_prob0:", wire_in.prob0)
    print("w_prob1:", wire_in.prob1)
    print("w_abs_prob:", wire_in.absprob)
    print("w_fanout:", wire_in.fanout)
    print("w_mainout:", wire_in.mainout, "\n")


def wire_fanin_cone(wire_in, cone_size):
    index_traverse = 0
    if (wire_in.type == "inp"):
        return []
    else:
        fanin_cone = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_cone[index_traverse].operands)
        while cone_size != 0:
            added_bef = 0
            if len(temp) == 0:
                index_traverse = index_traverse + 1
                if index_traverse >= len(fanin_cone):
                    cone_size = 0
                else:
                    temp = copy.deepcopy(fanin_cone[index_traverse].operands)
            elif temp[0].type == "inp":
                temp.remove(temp[0])
            else:
                for i in range(0, len(fanin_cone)):
                    if fanin_cone[i].name == temp[0].name:
                        added_bef = 1
                if added_bef == 0:
                    fanin_cone.append(temp[0])
                    cone_size = cone_size - 1
                temp.remove(temp[0])
    return fanin_cone


def wire_fanin_trace(wires, wire_in, cone_size):
    index_traverse = 0
    if (wire_in.type == "inp"):
        return []
    else:
        fanin_trace = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_trace[index_traverse].operands)
        while cone_size != 0:
            if len(temp) != 0:
                while temp[0].type == "marked":
                    temp.remove(temp[0])
                    if len(temp) == 0:
                        break

                if len(temp) == 0:
                    index_traverse = index_traverse + 1
                    if index_traverse >= len(fanin_trace):
                        return fanin_trace
                    else:
                        temp = copy.deepcopy(fanin_trace[index_traverse].operands)

                else:
                    added_before = 0
                    for i in range(0, len(fanin_trace)):
                        if fanin_trace[i].name == temp[0].name:
                            added_before = 1
                    if added_before == 0:
                        fanin_trace.append(temp[0])
                        for j in range(0, len(wires)):
                            if wires[j].name == temp[0].name:
                                if wires[j].type != "inp":
                                    wires[j].type = "marked"
                                    cone_size = cone_size - 1
                                    temp = copy.deepcopy(temp[0].operands)
                                else:
                                    temp.remove(temp[0])
                    else:
                        temp.remove(temp[0])
            else:
                temp = copy.deepcopy(fanin_trace[index_traverse].operands)

    return fanin_trace


def wire_fanin_low_skew_cone(wire_in, cone_size):
    if (wire_in.type == "inp"):
        return []
    else:
        fanin_cone = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_cone[0].operands)
        temp.sort(key=operator.attrgetter('absprob'))
        while cone_size != 0:
            before_added = 0
            if len(temp) == 0:
                return fanin_cone
            else:
                for i in range(0, len(fanin_cone)):
                    if fanin_cone[i].name == temp[0].name:
                        before_added = 1
                if before_added == 0 and temp[0].type != "inp":
                    fanin_cone.append(temp[0])
                    cone_size = cone_size - 1
                for j in range(0, len(temp[0].operands)):
                    temp.append(temp[0].operands[j])
                temp.remove(temp[0])
                temp.sort(key=operator.attrgetter('absprob'))

        if cone_size == 0:
            return fanin_cone


def wire_fanin_fanout_cone(wire_in, cone_size):
    if wire_in.type == "inp":
        return []
    else:
        fanin_cone = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_cone[0].operands)
        temp.sort(key=operator.attrgetter('fanout'))
        while cone_size != 0:
            before_added = 0
            equal = 1
            for i in range(0, len(temp)):
                for j in range(0, len(temp)):
                    if i!=j:
                        if temp[i].fanout != temp[j].fanout:
                            equal = 0
            if equal == 1:
                temp.sort(key=operator.attrgetter('absprob'), reverse=True)
            else:
                temp.sort(key=operator.attrgetter('fanout'))
            if len(temp) == 0:
                return fanin_cone
            else:
                for i in range(0, len(fanin_cone)):
                    if fanin_cone[i].name == temp[0].name:
                        before_added = 1
                if before_added == 0 and temp[0].type != "inp":
                    fanin_cone.append(temp[0])
                    cone_size = cone_size - 1
                for j in range(0, len(temp[0].operands)):
                    temp.append(temp[0].operands[j])
                temp.remove(temp[0])
                temp.sort(key=operator.attrgetter('absprob'), reverse=True)

        if cone_size == 0:
            return fanin_cone

def wire_fanin_mainout_cone(wire_in, cone_size):
    if wire_in.type == "inp":
        return []
    else:
        fanin_cone = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_cone[0].operands)
        temp.sort(key=operator.attrgetter('mainout'))
        while cone_size != 0:
            before_added = 0
            equal = 1
            for i in range(0, len(temp)):
                for j in range(0, len(temp)):
                    if i!=j:
                        if temp[i].fanout != temp[j].fanout:
                            equal = 0
            if equal == 1:
                temp.sort(key=operator.attrgetter('absprob'), reverse=True)
            else:
                temp.sort(key=operator.attrgetter('mainout'))
            if len(temp) == 0:
                return fanin_cone
            else:
                for i in range(0, len(fanin_cone)):
                    if fanin_cone[i].name == temp[0].name:
                        before_added = 1
                if before_added == 0 and temp[0].type != "inp":
                    fanin_cone.append(temp[0])
                    cone_size = cone_size - 1
                for j in range(0, len(temp[0].operands)):
                    temp.append(temp[0].operands[j])
                temp.remove(temp[0])
                temp.sort(key=operator.attrgetter('absprob'), reverse=True)

        if cone_size == 0:
            return fanin_cone

def wire_fanin_high_skew_cone(wire_in, cone_size):
    if (wire_in.type == "inp"):
        return []
    else:
        fanin_cone = [wire_in]
        cone_size = cone_size - 1
        temp = copy.deepcopy(fanin_cone[0].operands)
        temp.sort(key=operator.attrgetter('absprob'), reverse=True)
        while cone_size != 0:
            before_added = 0
            if len(temp) == 0:
                return fanin_cone
            else:
                for i in range(0, len(fanin_cone)):
                    if fanin_cone[i].name == temp[0].name:
                        before_added = 1
                if before_added == 0 and temp[0].type != "inp":
                    fanin_cone.append(temp[0])
                    cone_size = cone_size - 1
                for j in range(0, len(temp[0].operands)):
                    temp.append(temp[0].operands[j])
                temp.remove(temp[0])
                temp.sort(key=operator.attrgetter('absprob'), reverse=True)

        if cone_size == 0:
            return fanin_cone


def wire_dep(benchmark_address):
    inputs = []
    outputs = []
    wires = []
    input_array = []
    temp = []
    bench_file = open(benchmark_address)

    for line in bench_file:
        if "INPUT" in line:
            inputs.append(line[line.find("(") + 1:line.find(")")])
            input_array.append(line[line.find("(") + 1:line.find(")")])
            wires.append(wire(line[line.find("(") + 1:line.find(")")], "inp", [], "1", 0, 0.5, 0.5, 0, 0, 0))
            input_array = []
        elif "OUTPUT" in line:
            outputs.append(line[line.find("(") + 1:line.find(")")])
        elif " = " in line:
            gate_out = line[0: line.find(" =")]
            gate_type = line[line.find("= ") + 2: line.find("(")]
            gate_list_inputs = line[line.find("(") + 1:line.find(")")]
            gate_oprs = gate_list_inputs.split(", ")
            for i in range(0, len(gate_oprs)):
                for j in range(0, len(wires)):
                    if wires[j].name == gate_oprs[i]:
                        temp.append(wires[j])
                        break
            max_level = 0
            for i in range(0, len(temp)):
                if temp[i].logic_level > max_level:
                    max_level = temp[i].logic_level

            temp_prob0 = 0.25
            temp_prob1 = 0.25
            for i in range(0, len(temp)):
                if len(temp) == 1:
                    if gate_type == "NOT" or gate_type == "not":
                        temp_prob = temp[0].prob0
                        temp_prob0 = temp[0].prob1
                        temp_prob1 = temp_prob
                    elif gate_type == "BUFF" or gate_type == "buff":
                        temp_prob0 = temp[0].prob0
                        temp_prob1 = temp[0].prob1
                else:
                    if gate_type == "NAND" or gate_type == "nand":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob0 + temp_prob1 * temp[i].prob0 + temp_prob0 * temp[i].prob1
                            temp_prob1 = temp_prob1 * temp[i].prob1
                    elif gate_type == "AND" or gate_type == "and":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob0 + temp_prob1 * temp[i].prob0 + temp_prob0 * temp[i].prob1
                            temp_prob1 = temp_prob1 * temp[i].prob1
                    elif gate_type == "NOR" or gate_type == "nor":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob0
                            temp_prob1 = 1 - temp_prob0
                    elif gate_type == "OR" or gate_type == "or":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob0
                            temp_prob1 = 1 - temp_prob0
                    elif gate_type == "XNOR" or gate_type == "xnor":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob0 + temp_prob1 * temp[i].prob1
                            temp_prob1 = 1 - temp_prob0
                    elif gate_type == "XOR" or gate_type == "xor":
                        if i == 0:
                            temp_prob0 = temp[i].prob0
                            temp_prob1 = temp[i].prob1
                        else:
                            temp_prob0 = temp_prob0 * temp[i].prob1 + temp_prob1 * temp[i].prob0
                            temp_prob1 = 1 - temp_prob0

            if gate_type == "NAND" or gate_type == "nand":
                temp_prob = temp_prob0
                temp_prob0 = temp_prob1
                temp_prob1 = temp_prob

            if gate_type == "NOR" or gate_type == "nor":
                temp_prob = temp_prob0
                temp_prob0 = temp_prob1
                temp_prob1 = temp_prob

            wires.append(wire(gate_out, gate_type, temp, "1", max_level + 1, temp_prob0, temp_prob1, abs(temp_prob0 - temp_prob1), 0, 0))
            temp = []
    bench_file.close()

    for i in range(0, len(wires)):
        fanout_temp = 0
        for j in range(0, len(wires)):
            if i != j:
                for k in range(0, len(wires[j].operands)):
                    if wires[i].name == wires[j].operands[k].name:
                        fanout_temp = fanout_temp + 1
        wires[i].fanout = fanout_temp

    for i in range(0, len(outputs)):
        for j in range(0, len(wires)):
            if wires[j].name == outputs[i]:
                wire_list = wire_fanin_cone(wires[j], 4000)
        for j in range(0, len(wire_list)):
            for k in range(0, len(wires)):
                if wire_list[j].name == wires[k].name:
                    wires[k].mainout = wires[k].mainout + 1

    return wires


def cone_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_cone(wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "cone/marked/" + bench_file_name + "_cone_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def low_skew_cone_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_low_skew_cone(wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "coneskewl/marked/" + bench_file_name + "_coneskewl_LUT_" + str(
            gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def high_skew_cone_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_high_skew_cone(wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "coneskewh/marked/" + bench_file_name + "_coneskewh_LUT_" + str(
            gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def rnd_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))

        cpwires = copy.deepcopy(wires)
        marked = 0
        while marked != gate_num:
            rand_value = randint(0, len(cpwires) - 1)
            if cpwires[rand_value].type != "marked" and cpwires[rand_value].type != "inp":
                cpwires[rand_value].type = "marked"
                marked = marked + 1

        new_bench = ""
        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                for i in range(0, len(cpwires)):
                    if gate_out == cpwires[i].name:
                        if cpwires[i].type == "marked":
                            new_bench += line.replace(gate_type, "marked")
                        else:
                            new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "rnd/marked/" + bench_file_name + "_rnd_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def high_skew_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))

        cpwires = copy.deepcopy(wires)
        cpwires.sort(key=operator.attrgetter('absprob'), reverse=True)
        marked = 0
        m_index = 0
        while marked != gate_num:
            if cpwires[m_index] != "marked" and cpwires[m_index].type != "inp":
                cpwires[m_index].type = "marked"
                marked = marked + 1
            m_index = m_index + 1

        new_bench = ""
        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                for i in range(0, len(cpwires)):
                    if gate_out == cpwires[i].name:
                        if cpwires[i].type == "marked":
                            new_bench += line.replace(gate_type, "marked")
                        else:
                            new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "skewh/marked/" + bench_file_name + "_skewh_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def low_skew_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))

        cpwires = copy.deepcopy(wires)
        cpwires.sort(key=operator.attrgetter('absprob'))
        marked = 0
        m_index = 0
        while marked != gate_num:
            if cpwires[m_index] != "marked" and cpwires[m_index].type != "inp":
                cpwires[m_index].type = "marked"
                marked = marked + 1
            m_index = m_index + 1

        new_bench = ""
        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                for i in range(0, len(cpwires)):
                    if gate_out == cpwires[i].name:
                        if cpwires[i].type == "marked":
                            new_bench += line.replace(gate_type, "marked")
                        else:
                            new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "skewl/marked/" + bench_file_name + "_skewl_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def trace_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_trace(wires, wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "trace/marked/" + bench_file_name + "_trace_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def fanout_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_fanout_cone(wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "fanout/marked/" + bench_file_name + "_fanout_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


def mainout_marking(wires, to_be_lut_num, bench_address):
    bench_file_name = bench_address[bench_address.find("original/") + 9: bench_address.find(".bench")]
    bench_folder = bench_address[0: bench_address.find("original/")]

    max_cir = 0
    max_cir_ind = 0
    for i in range(0, len(wires)):
        curr_cir = len(wire_fanin_cone(wires[i], 4000))
        print("calculating fan-in cone of wire ", i, "(", wires[i].name, ") out of ", len(
            wires), " = ", curr_cir, "max = ", max_cir)
        if curr_cir > max_cir:
            max_cir = curr_cir
            max_cir_ind = i

    for gate_num in range(1, to_be_lut_num + 1):
        print("round ", gate_num, " out of ", str(to_be_lut_num))
        new_bench = ""
        wire_list = wire_fanin_mainout_cone(wires[max_cir_ind], gate_num)

        bench_file = open(bench_address)
        for line in bench_file:
            if "INPUT" in line:
                new_bench += line
            elif "OUTPUT" in line:
                new_bench += line
            elif " = " in line:
                gate_type = line[line.find("= ") + 2: line.find("(")]
                gate_out = line[0: line.find(" =")]
                found_line = 0
                for i in range(0, len(wire_list)):
                    if wire_list[i].name == gate_out:
                        found_line = 1
                if found_line == 1:
                    new_bench += line.replace(gate_type, "marked")
                else:
                    new_bench += line
            else:
                new_bench += line

        bench_file.close()

        new_bench_address = bench_folder + "mainout/marked/" + bench_file_name + "_mainout_LUT_" + str(gate_num) + ".bench"
        new_bench_file = open(new_bench_address, 'w')
        new_bench_file.write(new_bench)
        new_bench_file.close()

        print(new_bench_address)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='LUT marking Policies v1.0 (c) Hadi Mardani Kamali.')
    parser.add_argument("-b", action="store", required=True, type=str, help="benchmark circuit")
    parser.add_argument("-m", action="store", required=True, help="marking method")
    parser.add_argument("-n", action="store", required=True, type=int, help="number of markings")

    args = parser.parse_args()

    bench_circuit = args.b
    marking_policy = args.m

    bench_address = "../" + bench_circuit + "/original/" + bench_circuit + ".bench"
    to_be_lut_num = args.n

    wires = wire_dep(bench_address)

    # for i in range(0, len(wires)):
    #      wire_print(wires[i])

    if marking_policy == "random":
        rnd_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "lowskew":
        low_skew_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "highskew":
        high_skew_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "cone":
        cone_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "lowskewcone":
        low_skew_cone_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "highskewcone":
        high_skew_cone_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "fanout":
        fanout_marking(wires, to_be_lut_num, bench_address)
    elif marking_policy == "mainout":
        mainout_marking(wires, to_be_lut_num, bench_address)
    #
    # # rnd_marking(wires, to_be_lut_num, bench_address)
    # # low_skew_marking(wires, to_be_lut_num, bench_address)
    # # high_skew_marking(wires, to_be_lut_num, bench_address)
    # # cone_marking(wires, to_be_lut_num, bench_address)
    # # low_skew_cone_marking(wires, to_be_lut_num, bench_address)
    # # high_skew_cone_marking(wires, to_be_lut_num, bench_address)
    # # fanout_marking(wires, to_be_lut_num, bench_address)
    # mainout_marking(wires, to_be_lut_num, bench_address)

